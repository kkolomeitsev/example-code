/* eslint-disable @typescript-eslint/no-var-requires */

const { defaults } = require('jest-config');

module.exports = {
  rootDir: require('path').resolve(__dirname, '../../'),
  roots: [__dirname],
  preset: 'ts-jest',
  globals: {
    'ts-jest': {
      tsConfig: `${__dirname}/src/tsconfig.json`,
      isolatedModules: true,
      diagnostics: false,
    },
  },
  moduleNameMapper: {
    '^app(.*)$': `${__dirname}/src$1`,
  },
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/', `${__dirname}/node_modules/`],
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
  testMatch: [`${__dirname}/**/__tests__/**/*-test.(ts|js)`],
};
