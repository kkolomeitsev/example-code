module.exports = {
  client: {
    service: {
      name: 'client',
      localSchemaFile: './apps/schema.graphql',
    },
    tagName: 'omitGqlTagsTheyAreUnderGraphqlCodeGeneratorControl',
  },
};
