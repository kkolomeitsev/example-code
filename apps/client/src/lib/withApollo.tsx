import React from 'react';
import Head from 'next/head';
import { NextComponentType, NextPageContext } from 'next';
import { ApolloProvider } from '@apollo/react-hooks';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { split, ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import fetch from 'isomorphic-unfetch';
import { getConfig } from './getConfig';

const customFetch = (uri: string, options: any) => {
  const { operationName } = JSON.parse(options.body);
  return fetch(`${uri}?opname=${operationName}`, options);
};

interface WithApolloProps {
  apolloClient: ApolloClient<any> | null;
  apolloState: any;
  [key: string]: any;
}

export interface WithApolloContext extends NextPageContext {
  apolloClient: ApolloClient<any>;
}

export function withApollo(PageComponent: NextComponentType<any, any, any>, { ssr = true } = {}) {
  const WithApollo = ({ apolloClient, apolloState, ...pageProps }: WithApolloProps) => {
    const client = apolloClient || initApolloClient(apolloState);
    return (
      <ApolloProvider client={client}>
        <PageComponent {...pageProps} />
      </ApolloProvider>
    );
  };

  if (process.env.NODE_ENV !== 'production') {
    const displayName = PageComponent.displayName || PageComponent.name || 'Component';

    if (displayName === 'App') {
      console.warn('This withApollo HOC only works with PageComponents.');
    }

    WithApollo.displayName = `withApollo(${displayName})`;
  }

  if (ssr || PageComponent.getInitialProps) {
    WithApollo.getInitialProps = async (ctx: WithApolloContext) => {
      const { AppTree, req, res } = ctx;

      const fetchOptions: any = {};

      if (req) {
        const cookie = req.headers['cookie'];
        const origin = req.headers['origin'];
        fetchOptions.headers = { cookie, origin };
      }

      const apolloClient = (ctx.apolloClient = initApolloClient({}, fetchOptions, ctx));

      let pageProps = {};
      if (PageComponent.getInitialProps) {
        pageProps = await PageComponent.getInitialProps(ctx);
      }

      if (typeof window === 'undefined') {
        if (res && res.finished) {
          return pageProps;
        }

        if (ssr) {
          try {
            const { getDataFromTree } = await import('@apollo/react-ssr');
            await getDataFromTree(
              <AppTree
                pageProps={{
                  ...pageProps,
                  apolloClient,
                }}
              />
            );
          } catch (error) {
            console.error('Error while running `getDataFromTree`', error);
          }

          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          Head.rewind();
        }
      }

      const apolloState = apolloClient.cache.extract();

      return {
        ...pageProps,
        apolloState,
      };
    };
  }

  return WithApollo;
}

let apolloClientInBrowser: ApolloClient<any> | null = null;

function initApolloClient(
  initialState: Record<string, any> = {},
  fetchOptions: Record<string, any> = {},
  ctx?: NextPageContext
) {
  // for SSR create every time new ApolloClient
  if (typeof window === 'undefined') {
    return createApolloClient(initialState, fetchOptions, ctx);
  }

  // for Browser try to reuse global instance
  if (!apolloClientInBrowser) {
    apolloClientInBrowser = createApolloClient(initialState, {}, ctx);
  }
  return apolloClientInBrowser;
}

export function createApolloClient(
  initialState: Record<string, any> = {},
  { headers }: Record<string, any> = {},
  _ctx?: NextPageContext
) {
  const linkOptions: HttpLink.Options = {
    uri: getConfig('APP_GRAPHQL_URI'),
    credentials: 'include',
    fetch: customFetch,
  };
  if (headers) linkOptions.headers = headers;

  const ssrMode = typeof window === 'undefined';
  const httpLink = new HttpLink(linkOptions);

  let link: ApolloLink;

  if (ssrMode) {
    link = httpLink;
  } else {
    const wsLink = new WebSocketLink({
      uri: getConfig('APP_GRAPHQL_WS_URI'),
      options: {
        reconnect: true,
      },
    });

    link = split(
      // split based on operation type
      ({ query }) => {
        const definition = getMainDefinition(query);
        return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
      },
      wsLink,
      httpLink
    );
  }

  return new ApolloClient({
    ssrMode,
    link,
    cache: new InMemoryCache().restore(initialState),
  });
}
