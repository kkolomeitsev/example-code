import Router from 'next/router';

export default (context: any, target: string) => {
  if (context.res) {
    if (!context.res.headersSent) context.res.redirect(target);
  } else {
    Router.replace(target);
  }
};
