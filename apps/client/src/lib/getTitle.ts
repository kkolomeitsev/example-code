export function getTitle(value: string | undefined | null): string {
  if (typeof value !== 'string') {
    return '';
  }

  let title = '';

  switch (value.trim().toLowerCase()) {
    case 'open':
      title = 'Открыто';
      break;
    case 'closed':
      title = 'Закрыто';
      break;
    case 'break':
      title = 'Перерыв';
      break;
    default:
      return '';
  }

  return title;
}
