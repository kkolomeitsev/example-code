import { useQuery, QueryHookOptions as Q } from '@apollo/react-hooks';
import { DocumentNode as D, GraphQLError } from 'graphql';
import { NotificationWithIcon, Notification } from '../components/NotificationWithIcon';

export function useQueryWithNotification<R, V>(
  query: D,
  opts?: Q<R, V>,
  notificationOptionsError?: Notification | ReadonlyArray<GraphQLError> | Error | any
) {
  const errorOptions = notificationOptionsError || {
    type: 'error',
    message: 'Ошибка при получении данных',
    title: 'Ошибка!',
  };
  const queryResult = useQuery<R, V>(query, {
    ...opts,
    onError: (error) => {
      if (error) {
        if (error.graphQLErrors) {
          error.graphQLErrors.forEach((err) => {
            NotificationWithIcon({ ...errorOptions, message: err.message });
          });
        } else if (error.networkError) {
          NotificationWithIcon({ ...errorOptions, message: error.message });
        }
      }
    },
  });

  return queryResult;
}
