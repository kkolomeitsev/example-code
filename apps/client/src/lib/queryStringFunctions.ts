import { SingletonRouter } from 'next/router';

export function setRouterParams(router: SingletonRouter, params: any) {
  let route = '/';

  const str = [];
  for (const p in params)
    if (params.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(params[p]));
    }
  const query = str.join('&');

  if (query.length) route += `?${query}`;

  router.push(route);
}
