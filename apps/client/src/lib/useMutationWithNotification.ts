import { useMutation, MutationHookOptions as M, MutationTuple } from '@apollo/react-hooks';
import { DocumentNode as D, GraphQLError } from 'graphql';
import { NotificationWithIcon, Notification } from '../components/NotificationWithIcon';

export function useMutationWithNotification<R, V>(
  mutation: D,
  opts?: M<R, V>,
  notificationOptionsSuccess?: Notification,
  notificationOptionsError?: Notification | ReadonlyArray<GraphQLError> | any
): MutationTuple<R, V> {
  const successOptions = notificationOptionsSuccess || {
    type: 'success',
    message: '',
    title: 'Данные обновлены',
  };
  const errorOptions = notificationOptionsError || {
    type: 'error',
    message: 'Ошибка при обновлении данных',
    title: 'Ошибка!',
  };
  const [triggerMutation, mutationResult] = useMutation<R, V>(mutation, {
    ...opts,
    onCompleted: (data) => {
      if (data) {
        NotificationWithIcon({ ...successOptions });
      }
    },
    onError: (error) => {
      if (error) {
        if (error.graphQLErrors) {
          error.graphQLErrors.forEach((err) => {
            NotificationWithIcon({ ...errorOptions, message: err.message });
          });
        } else if (error.networkError)
          NotificationWithIcon({ ...errorOptions, message: error.message });
      }
    },
  });

  return [triggerMutation, mutationResult];
}
