export function getErrorMessage(value: string | undefined | null): string {
  if (typeof value !== 'string') {
    return '';
  }

  const titleArr = value.split('GraphQL error: ');

  let title = '';

  switch ((titleArr?.[1] || titleArr?.[0] || '').trim()) {
    case 'You are trying to schedule on past date':
      title = 'Вы пытаетесь записаться на прошедшую дату';
      break;
    case "In this queue you can't sign up so far to the future":
      title = 'Дата записи слишком далеко';
      break;
    case "Queue is closed, you can't create in live entry":
      title = 'Очередь закрыта. Вы не можете сразу встать в живую очередь';
      break;
    case "Queue is closed, it's a day off":
      title = 'Запись на этот день закрыта, выходной';
      break;
    case 'You are trying to schedule on non-working hours':
      title = 'Вы пытаетесь заприсаться на внерабочее время';
      break;
    case 'You already has entry in this queue on this day':
      title = 'Вы уже записаны на этот день';
      break;
    case 'For this queue day limit is full':
      title = 'Количество записей на этот день достигло максимума, запись закрыта';
      break;
    case 'For this queue limit is full':
      title = 'Количество записей в эту очередь достигло максимума, запись закрыта';
      break;
    default:
      return '';
  }

  return title;
}
