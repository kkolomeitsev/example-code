export default function windowWidth() {
  let size = 0;
  if (typeof window !== 'undefined') {
    size = window.outerWidth;
  }
  return size;
}
