import React, { useState } from 'react';
import { Alert, Form, Button, Input, Spin, Typography } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import MaskedInputPhone from '../MaskedInputPhone';
import {
  useRegisterUserMutation,
  RegisterUserMutationVariables as V,
} from './__generated__/RegisterUserMutation';

const { Text } = Typography;
const { Password } = Input;

function RegisterUser() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const router = useRouter();
  const [form] = Form.useForm();

  const returnUrl = Array.isArray(router.query['return_url'])
    ? router.query['return_url'].pop()
    : router.query['return_url'];

  const [mutate] = useRegisterUserMutation({
    notifications: { onCompleted: false },
  });

  const handleSubmit = async (values: any) => {
    setError('');
    setLoading(true);
    const input: V['input'] = {} as any;
    input['title'] = values.title;
    input['phone'] = values.phone;
    input['password'] = values.password;
    input['username'] = values.username;
    const result = await mutate({ variables: { input } });
    setLoading(false);
    if (result?.data?.queue?.register?._id) {
      setTimeout(() => {
        router.push(`/login${returnUrl ? `?return_url=${encodeURI(returnUrl)}` : ''}`);
      }, 300);
    } else {
      setError('Ошибка регистрации');
    }
  };

  const beforeSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    form
      .validateFields()
      .then((values) => {
        handleSubmit(values);
      })
      .catch((_err) => null);
  };

  return (
    <Spin spinning={loading}>
      {error ? <Alert message={error} className="form-alert" type="error" /> : null}
      <Form
        className="login-form"
        layout="vertical"
        onFinish={handleSubmit}
        hideRequiredMark
        form={form}
      >
        <Form.Item
          label="Email адрес"
          name="username"
          rules={[
            { required: true, message: 'Это обязательное поле!' },
            { type: 'email', message: 'Неправильный email адрес' },
          ]}
        >
          <Input placeholder="Ваш email" />
        </Form.Item>
        <Form.Item
          label="Имя"
          name="title"
          rules={[{ required: true, message: 'Это обязательное поле!' }]}
        >
          <Input placeholder="Имя" />
        </Form.Item>
        <Form.Item
          label="Номер телефона"
          name="phone"
          rules={[{ required: true, message: 'Это обязательное поле!' }]}
        >
          <MaskedInputPhone placeholder="+7 ..." />
        </Form.Item>
        <Form.Item
          label="Пароль"
          name="password"
          rules={[{ required: true, message: 'Это обязательное поле!' }]}
        >
          <Password placeholder="Пароль" />
        </Form.Item>
        <Text type="secondary">
          Нажимая «Регистрация», вы принимаете наши
          <br />
          <Link href="/terms/privacy">
            <a>Политику конфиденциальности</a>
          </Link>{' '}
          и{' '}
          <Link href="/terms/common">
            <a>Пользовательское соглашение</a>
          </Link>
        </Text>
      </Form>
      <Button onClick={beforeSubmit} type="primary" size="large" className="button-large-with">
        Регистрация
      </Button>
    </Spin>
  );
}

export default RegisterUser;
