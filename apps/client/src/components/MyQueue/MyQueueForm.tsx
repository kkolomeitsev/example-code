import React, { useState, useEffect } from 'react';
import {
  Drawer,
  Alert,
  Form,
  Button,
  Input,
  InputNumber,
  Spin,
  TimePicker,
  Select,
  Switch,
} from 'antd';
import moment from 'moment';
import { SettingOutlined, PlusOutlined } from '@ant-design/icons';
import windowWidth from 'app/lib/windowWidth';
import { Queue_data as Data } from 'app/components/__generated__/Queue_data';

const { TextArea } = Input;

const { Option } = Select;

const { RangePicker } = TimePicker;

const days: { [key: string]: string } = {
  d1: 'Понедельник',
  d2: 'Вторник',
  d3: 'Среда',
  d4: 'Четверг',
  d5: 'Пятница',
  d6: 'Суббота',
  d7: 'Воскресенье',
};

interface P {
  submit: (values: Partial<Data>) => Promise<boolean>;
  data?: Data;
}

function MyQueueForm(props: P) {
  const { submit, data } = props;

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();

  const [init, setInit] = useState(false);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (init && typeof window !== 'undefined') {
      setWidth(windowWidth() || 1);
    }
  }, [init]);

  if (!init && typeof window !== 'undefined') {
    setInit(true);
  }

  const onClose = () => {
    setVisible(false);
    form.resetFields();
  };

  const handleSubmit = (values: any) => {
    if (loading) {
      return;
    }
    setError('');
    setLoading(true);
    submit(values)
      .then((result) => {
        if (result) {
          setTimeout(() => {
            onClose();
          }, 500);
        }
      })
      .catch((e) => {
        setError(`Ошибка${e.message ? `: ${e.message}` : '!'}`);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const beforeSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    form
      .validateFields()
      .then((values) => {
        handleSubmit(values);
      })
      .catch((_err) => null);
  };

  if (data?.workingHours) {
    const timeFrom = data.workingHours?.[0] || '00:00:00.000';
    const timeTo = data.workingHours?.[1] || '23:59:59.999';
    const normalizedHours = [moment(timeFrom, 'HH:mm:ss.SSS'), moment(timeTo, 'HH:mm:ss.SSS')];
    data.workingHours = normalizedHours as any;
  }

  const childrenDays: JSX.Element[] = [];

  for (let i = 1; i < 7; i++) {
    const iVal = String(i);
    childrenDays.push(
      <Option key={iVal} value={iVal}>
        {days[`d${iVal}`] || 'Ошибка'}
      </Option>
    );
  }

  return (
    <>
      {data ? (
        <Button type="link" onClick={() => setVisible(true)}>
          <SettingOutlined /> Настроить
        </Button>
      ) : (
        <Button type="primary" onClick={() => setVisible(true)}>
          <PlusOutlined /> Создать
        </Button>
      )}
      <Drawer
        title="Моя очередь"
        width={width && width < 768 ? 250 : 450}
        visible={visible}
        onClose={onClose}
      >
        <Spin spinning={loading}>
          {error ? <Alert message={error} className="form-alert" type="error" /> : null}
          <Form
            className="queue-form"
            layout="vertical"
            onFinish={handleSubmit}
            hideRequiredMark
            form={form}
            initialValues={
              data || {
                gmt: 6,
                workingHours: [
                  moment('00:00:00.000', 'HH:mm:ss.SSS'),
                  moment('23:59:59.999', 'HH:mm:ss.SSS'),
                ],
              }
            }
          >
            <Form.Item
              label="Название очереди"
              name="title"
              rules={[{ required: true, message: 'Это обязательное поле!' }]}
            >
              <Input placeholder="Название" />
            </Form.Item>
            <Form.Item label="Описание для клиентов" name="description">
              <TextArea placeholder="Описание" />
            </Form.Item>
            <Form.Item label="Рабочие часы" name="workingHours">
              <RangePicker picker="time" format="HH:mm" />
            </Form.Item>
            <Form.Item label="Еженедельные выходные" name="recurringWeekend">
              <Select mode="multiple" style={{ width: '100%' }} placeholder="Выберите выходные дни">
                {childrenDays}
              </Select>
            </Form.Item>
            <Form.Item label="Среднее время обслуживания (минуты)" name="averageServiceMinutes">
              <InputNumber placeholder="Количество минут" min={1} />
            </Form.Item>
            <Form.Item label="Ежедневная вместимость очереди" name="dayLimit">
              <InputNumber placeholder="Ежедневная вместимость" min={0} />
            </Form.Item>
            <Form.Item label="Максимальная вместимость очереди" name="commonLimit">
              <InputNumber placeholder="Максимальная вместимость" min={0} />
            </Form.Item>
            <Form.Item
              label="Ограничивать живую очередь"
              name="limitLifeQueue"
              valuePropName="checked"
            >
              <Switch />
            </Form.Item>
            <Form.Item label="Доступно бронирование (дней вперед)" name="bookingPeriodDays">
              <InputNumber placeholder="Количество дней" min={0} />
            </Form.Item>
            <Form.Item label="Часовой пояс" name="gmt">
              <InputNumber placeholder="Часовой пояс" />
            </Form.Item>
          </Form>
          <br />
          <br />
          <br />
        </Spin>
        <div className="drawer-form-footer">
          <Button
            onClick={beforeSubmit}
            type="primary"
            size="large"
            className="button-large-with"
            disabled={loading}
          >
            Сохранить
          </Button>
          <Button onClick={() => onClose()} style={{ marginLeft: 18 }}>
            Отмена
          </Button>
        </div>
      </Drawer>
    </>
  );
}

export default MyQueueForm;
