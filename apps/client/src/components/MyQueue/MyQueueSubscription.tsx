import React from 'react';
import { NotificationWithIcon } from '../NotificationWithIcon';
import { useMyQueueSubscription } from './__generated__/MyQueueSubscription';

function MyQueueSubscription(props: { queue: string; refetch: () => void }) {
  const { queue, refetch } = props;

  useMyQueueSubscription({
    variables: { queue },
    onSubscriptionData: ({ subscriptionData }) => {
      if (subscriptionData?.data?.myQueue?.upd) {
        refetch();
      }
      const message = subscriptionData?.data?.myQueue?.msg;
      if (message) {
        NotificationWithIcon({ type: 'success', title: 'Обновление', message });
      }
    },
  });

  return <span />;
}

export default MyQueueSubscription;
