import React from 'react';
import moment, { Moment } from 'moment';
import MyQueueForm from './MyQueueForm';
import { Queue_data as Data } from 'app/components/__generated__/Queue_data';
import {
  useCreateMyQueueMutation,
  CreateMyQueueMutationVariables as V,
} from './__generated__/CreateMyQueueMutation';

function CreateMyQueue() {
  const [mutate] = useCreateMyQueueMutation({
    refetchQueries: ['MyQueueQuery'],
    notifications: { onCompleted: { title: 'Ваша очередь создана' } },
  });

  return (
    <MyQueueForm
      submit={async (values: Partial<Data>) => {
        const input: V['input'] = {
          title: '',
          limitLifeQueue: undefined,
          description: undefined,
          recurringWeekend: undefined,
          gmt: undefined,
          commonLimit: undefined,
          dayLimit: undefined,
          bookingPeriodDays: undefined,
          averageServiceMinutes: undefined,
        };
        for (const key in input) {
          const value = (values as any)[key];
          if (typeof value !== 'undefined') {
            (input as any)[key] = value;
          } else if (!(input as any)[key]) {
            if (typeof (input as any)[key] !== 'undefined') {
              throw new Error('Заполнены не все обязательные поля!');
            }
            delete (input as any)[key];
          }
        }
        if (values.workingHours) {
          const hourFrom: Moment = moment(values.workingHours?.[0] || '00:00:00.000');
          const hourTo: Moment = moment(values.workingHours?.[1] || '23:59:59.999');
          input['workingHours'] = [
            hourFrom.startOf('minute').format('HH:mm:ss.sss'),
            hourTo.startOf('minute').format('HH:mm:ss.sss'),
          ];
        }
        await mutate({ variables: { input } });
        return true;
      }}
    />
  );
}

export default CreateMyQueue;
