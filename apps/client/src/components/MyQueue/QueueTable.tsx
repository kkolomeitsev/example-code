import React from 'react';
import { Table, Popover } from 'antd';
import { ColumnProps } from 'antd/lib/table';
import { CheckCircleOutlined, MinusCircleOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { MasterEntry_data as Data } from 'app/components/__generated__/MasterEntry_data';
import moment from 'moment';

export default function QueueTable(props: { data: Data[] }) {
  const inLifeData: Data[] = [];
  const notInData: Data[] = [];
  props.data
    .sort((a, b) => (moment(a.date).isBefore(b.date) ? -1 : moment(a.date).isSame(b.date) ? 0 : 1))
    .forEach((el) => {
      if (el.inLife) {
        inLifeData.push(el);
      } else {
        notInData.push(el);
      }
    });

  const data = inLifeData.concat(notInData);

  const columns: ColumnProps<Data>[] = [
    {
      title: 'Записался на',
      key: 'time',
      align: 'left',
      width: '35%',
      render: (_, record) => (
        <span>
          <b>{moment(record.date).format('HH:mm')}</b>
        </span>
      ),
    },
    {
      title: 'Номер',
      dataIndex: 'commonTurn',
      key: 'commonTurn',
      width: '25%',
      render: (value: string) => (
        <span>
          <b>{value}</b>
        </span>
      ),
    },
    {
      title: 'Пришёл',
      dataIndex: 'inLife',
      key: 'inLife',
      width: '25%',
      align: 'center',
      render: (value: boolean) =>
        value ? (
          <CheckCircleOutlined className="color-yes ico-lg" />
        ) : (
          <MinusCircleOutlined className="color-not ico-lg" />
        ),
    },

    {
      title: '',
      key: 'info',
      align: 'center',
      render: (_, record) => (
        <Popover
          content={
            <div>
              <p>{record.user?.title || 'Имя неизвестно'}</p>
              <p>{record.user?.phone || 'Телефона нет'}</p>
              <p>{record.user?.username}</p>
              {record.description ? <p>{record.description}</p> : null}
            </div>
          }
          title="Информация"
          trigger="click"
        >
          <InfoCircleOutlined className="ico-lg" />
        </Popover>
      ),
    },
  ];

  return props.data.length ? (
    <Table
      dataSource={data}
      rowKey="_id"
      columns={columns}
      pagination={false}
      locale={{ emptyText: 'Очередь пуста' }}
    />
  ) : (
    <p className="empty-queue-table-text">Очередь пуста</p>
  );
}
