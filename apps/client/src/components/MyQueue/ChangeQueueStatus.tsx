import React, { useState } from 'react';
import { Button, Modal, Input } from 'antd';
import {
  StopOutlined,
  QuestionOutlined,
  PlayCircleOutlined,
  PauseCircleOutlined,
  ExclamationCircleTwoTone,
  LoadingOutlined,
} from '@ant-design/icons';
import {
  useChangeQueueStatusMutation,
  ChangeQueueStatusMutationVariables as V,
} from './__generated__/ChangeQueueStatusMutation';

const { TextArea } = Input;

function ChangeQueueStatus(props: V['input']) {
  const [mutate, { loading }] = useChangeQueueStatusMutation({
    refetchQueries: ['MyQueueQuery'],
    notifications: { onCompleted: { title: 'Сделано!' } },
  });

  const [visible, setVisible] = useState(false);
  const [breakMessage, setBreakMessage] = useState(props.breakMessage || '');

  function showModal() {
    setVisible(true);
  }

  function handleOk(loading: boolean) {
    if (loading) {
      return;
    }
    mutate({ variables: { input: { ...props, breakMessage } } });
    setVisible(false);
    setBreakMessage(props.breakMessage || '');
  }

  function handleCancel() {
    setVisible(false);
    setBreakMessage(props.breakMessage || '');
  }

  function closeHandleOk(loading: boolean) {
    if (loading) {
      return;
    }
    mutate({ variables: { input: props } });
    setVisible(false);
  }

  function closeHandleCancel() {
    setVisible(false);
  }

  let text = '';
  let breakModal = false;
  let closeModal = false;
  let ico = <QuestionOutlined />;

  switch (props.status) {
    case 'OPEN':
      text = 'Открыть';
      ico = <PlayCircleOutlined />;
      break;
    case 'CLOSED':
      text = 'Закрыть';
      closeModal = true;
      ico = <StopOutlined />;
      break;
    case 'BREAK':
      text = 'Перерыв';
      breakModal = true;
      ico = <PauseCircleOutlined />;
      break;
  }

  return (
    <>
      <Button
        type="link"
        onClick={() => {
          if (breakModal || closeModal) {
            showModal();
          } else {
            if (!loading) {
              mutate({ variables: { input: props } });
            }
          }
        }}
      >
        {loading ? <LoadingOutlined /> : ico} {text}
      </Button>
      {!breakModal ? null : (
        <Modal
          title="Перерыв"
          visible={visible}
          onOk={() => handleOk(loading)}
          onCancel={handleCancel}
          cancelText="Отмена"
          okText="Сделать перерыв"
        >
          <p>Сообщение для клиентов, на время перерыва...</p>
          <TextArea
            placeholder="Сообщение"
            value={breakMessage}
            onChange={(e) => {
              setBreakMessage(e.target.value);
            }}
          />
        </Modal>
      )}
      {!closeModal ? null : (
        <Modal
          title="Закрытие очереди"
          visible={visible}
          onOk={() => closeHandleOk(loading)}
          onCancel={closeHandleCancel}
          cancelText="Отмена"
          okText="Закрыть очередь"
        >
          <ExclamationCircleTwoTone className="d-inline modal-icon" twoToneColor="#fadb14" />
          <div className="d-inline">
            <p>Закрытие очереди обозначает закрытие рабочего дня</p>
            <p>Живая очередь будет распущена</p>
          </div>
        </Modal>
      )}
    </>
  );
}

export default ChangeQueueStatus;
