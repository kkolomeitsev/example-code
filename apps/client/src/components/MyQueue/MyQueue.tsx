import React from 'react';
import { Spin, Typography, Col, Row, Card, Divider } from 'antd';
import NotAuthLinks from 'app/components/NotAuthLinks';
import { getTitle } from 'app/lib/getTitle';
import CreateMyQueue from './CreateMyQueue';
import UpdateMyQueue from './UpdateMyQueue';
import ChangeQueueStatus from './ChangeQueueStatus';
import ChangeQueueEntry from './ChangeQueueEntry';
import EntryCard from './EntryCard';
import QueueTable from './QueueTable';
import MyQueueSubscription from './MyQueueSubscription';
import { useMyQueueQuery } from './__generated__/MyQueueQuery';
import { QueueStatus } from 'app/__generated__/types';
import { MasterEntry_data as QueueData } from 'app/components/__generated__/MasterEntry_data';

const { Title, Text } = Typography;

function MyQueue() {
  const { data: result, loading, refetch } = useMyQueueQuery();

  const data = result?.queue?.profile?.queue || null;
  const isAuth = result?.queue?.profile?._id;

  const actions: any[] = [];
  let cardActions: any[] = [];

  if (!loading && !isAuth) {
    return <NotAuthLinks />;
  }

  if (data) {
    switch (data?.status) {
      case 'OPEN':
        actions.push(
          <ChangeQueueStatus
            key="break-queue-status"
            breakMessage={data.breakMessage || ''}
            status={QueueStatus.BREAK}
          />
        );
        actions.push(<ChangeQueueStatus key="stop-queue-status" status={QueueStatus.CLOSED} />);
        break;
      case 'BREAK':
        actions.push(<ChangeQueueStatus key="start-queue-status" status={QueueStatus.OPEN} />);
        break;
      case 'CLOSED':
        actions.push(<ChangeQueueStatus key="start-queue-status" status={QueueStatus.OPEN} />);
        break;
    }

    actions.push(<UpdateMyQueue key="update-queue-action" data={data} />);

    const hasCurrent = data.currentEntry ? true : false;
    const hasQueue = data.totalEntries?.length ? true : false;

    cardActions = [
      <ChangeQueueEntry
        key="entry-done-next"
        inviteNext={true}
        hasCurrent={hasCurrent}
        hasQueue={hasQueue}
      />,
      <div key="buttons-block">
        <ChangeQueueEntry key="entry-not-came" currentNotCome={true} hasCurrent={hasCurrent} />
        <Divider key="bb-divider" className="custom-divider-action" />
        <ChangeQueueEntry key="entry-done" stopCurrent={true} hasCurrent={hasCurrent} />
      </div>,
    ];
  }

  return (
    <>
      <Title className="page-title" level={4}>
        Управление очередью
      </Title>
      {data?.link && !loading ? <MyQueueSubscription queue={data.link} refetch={refetch} /> : null}
      <Spin spinning={loading}>
        {loading ? null : !data ? (
          <CreateMyQueue />
        ) : (
          <>
            <Row gutter={[{ md: 16, lg: 16, xl: 32 }, 16]}>
              <Col span={24} md={12} lg={12} xl={8}>
                <Card
                  title={
                    <Title className="queue-title-l2" level={3}>
                      Текущая запись
                    </Title>
                  }
                  actions={cardActions.length ? cardActions : undefined}
                >
                  <EntryCard data={data} />
                </Card>
              </Col>
              <Col span={24} md={12} lg={12} xl={8}>
                <Card
                  title={
                    <Row>
                      <Col span={24} md={15} lg={15} xl={15}>
                        <Title className="queue-title-l2" level={3}>
                          {data.title || ''}
                        </Title>
                      </Col>
                      <Col span={24} md={9} lg={9} xl={9} className="card-title-simple">
                        ссылка:{' '}
                        <Text
                          copyable={{
                            text: `${
                              typeof window !== 'undefined' ? window.location.origin : ''
                            }/queue/${data.link}`,
                          }}
                          strong
                        >
                          {data.link}
                        </Text>
                      </Col>
                    </Row>
                  }
                  actions={actions.length ? actions : undefined}
                >
                  <Row>
                    <Col span={24} md={15} lg={15} xl={15}>
                      Сейчас в живой очереди:{' '}
                      <span className="bigger-number">{data.entries?.length || 0}</span>
                      <br />
                      Осталось на сегодня:{' '}
                      <span className="bigger-number">{data.totalEntries?.length || 0}</span>
                    </Col>
                    <Col span={24} md={9} lg={9} xl={9}>
                      Статус:{' '}
                      <span
                        className={`queue-status-title status-${
                          data.status?.trim().toLowerCase() || 'closed'
                        }`}
                      >
                        {getTitle(data.status).toUpperCase()}
                      </span>
                    </Col>
                  </Row>
                </Card>
              </Col>
              <Col span={24} md={12} lg={12} xl={8}>
                <Card
                  title={
                    <Title className="queue-title-l2" level={3}>
                      Очередь
                    </Title>
                  }
                  className="queue-table-card"
                >
                  <QueueTable data={(data.totalEntries as QueueData[]) || []} />
                </Card>
              </Col>
            </Row>
          </>
        )}
      </Spin>
    </>
  );
}

export default MyQueue;
