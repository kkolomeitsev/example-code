import React from 'react';
import { Row, Col } from 'antd';
import { Queue_data as Data } from 'app/components/__generated__/Queue_data';

export default function EntryCard(props: { data: Data }) {
  if (!props.data.currentEntry) {
    return <div>Сейчас никто не приглашен</div>;
  }
  return (
    <Row>
      <Col span={12}>
        <p>Номер:</p>
        <p>
          <span className={`queue-turn-title`}>{props.data.currentEntry.commonTurn}</span>
        </p>
      </Col>
      <Col span={12}>
        <p>Данные:</p>
        <div className="card-description">
          <p>
            <b>{props.data.currentEntry.user?.title}</b>
          </p>
          <p>{props.data.currentEntry.user?.phone}</p>
          <p>{props.data.currentEntry.user?.username}</p>
          {props.data.currentEntry.description ? (
            <p>{props.data.currentEntry.description}</p>
          ) : null}
        </div>
      </Col>
    </Row>
  );
}
