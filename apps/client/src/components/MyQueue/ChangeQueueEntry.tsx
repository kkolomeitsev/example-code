import React from 'react';
import { Button } from 'antd';
import {
  UserDeleteOutlined,
  QuestionOutlined,
  StepForwardOutlined,
  CheckCircleOutlined,
  LoadingOutlined,
} from '@ant-design/icons';
import {
  useChangeQueueEntryMutation,
  ChangeQueueEntryMutationVariables as V,
} from './__generated__/ChangeQueueEntryMutation';

function ChangeQueueEntryMutation(props: V & { hasCurrent: boolean; hasQueue?: boolean }) {
  const [mutate, { loading }] = useChangeQueueEntryMutation({
    refetchQueries: ['MyQueueQuery'],
    notifications: { onCompleted: { title: 'Сделано!' } },
  });

  const { currentNotCome, inviteNext, stopCurrent, hasCurrent, hasQueue } = props;

  let text: string | JSX.Element = '';
  let classButton = '';
  let ico = <QuestionOutlined />;

  if (currentNotCome) {
    text = 'Отложить';
    ico = <UserDeleteOutlined />;
    classButton = 'not-came-button';
  } else if (inviteNext) {
    text = (
      <div className="custom-icon-titles">
        Обработан
        <br />
        Cледующий
      </div>
    );
    ico = <StepForwardOutlined className="custom-icon-action" />;
    classButton = 'success-next-button';
  } else if (stopCurrent) {
    text = 'Обработан';
    ico = <CheckCircleOutlined />;
    classButton = 'success-button';
  }

  return (
    <Button
      type="link"
      className={classButton || undefined}
      onClick={() => {
        if (hasCurrent || (inviteNext && hasQueue)) {
          if (!loading) {
            mutate({ variables: props });
          }
        }
      }}
    >
      {loading ? <LoadingOutlined /> : ico} {text}
    </Button>
  );
}

export default ChangeQueueEntryMutation;
