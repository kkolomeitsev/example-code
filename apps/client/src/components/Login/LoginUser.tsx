import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { Alert, Switch, Form, Button, Input, Spin } from 'antd';
import {
  useLoginUserMutation,
  LoginUserMutationVariables as V,
} from './__generated__/LoginUserMutation';

const { Password } = Input;

function LoginUser() {
  const router = useRouter();

  const returnUrl = Array.isArray(router.query['return_url'])
    ? router.query['return_url'].pop()
    : router.query['return_url'];

  const redirectUrl = returnUrl || '/';

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const [form] = Form.useForm();

  const [mutate] = useLoginUserMutation({
    notifications: { onCompleted: false },
  });

  const handleSubmit = async (values: any) => {
    setError('');
    setLoading(true);
    const variables: V = {} as any;
    variables['username'] = values.username;
    variables['password'] = values.password;
    variables['remember'] = values.remember;
    const result = await mutate({ variables });
    setLoading(false);
    if (result?.data?.queue?.login?._id) {
      setTimeout(() => {
        window.location.replace(redirectUrl);
      }, 300);
    } else {
      setError('Ошибка авторизации');
    }
  };

  const beforeSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    form
      .validateFields()
      .then((values) => {
        handleSubmit(values);
      })
      .catch((_err) => null);
  };

  return (
    <Spin spinning={loading}>
      {error ? <Alert message={error} className="form-alert" type="error" /> : null}
      <Form
        className="login-form"
        layout="vertical"
        onFinish={handleSubmit}
        hideRequiredMark
        form={form}
        initialValues={{ remember: true }}
      >
        <Form.Item
          label="Email адрес"
          name="username"
          rules={[
            { required: true, message: 'Это обязательное поле!' },
            { type: 'email', message: 'Неправильный email адрес' },
          ]}
        >
          <Input placeholder="Ваш email" />
        </Form.Item>
        <Form.Item
          label="Пароль"
          name="password"
          rules={[{ required: true, message: 'Это обязательное поле!' }]}
        >
          <Password placeholder="Пароль" />
        </Form.Item>
        <Form.Item label="Запомнить меня" name="remember" valuePropName="checked">
          <Switch />
        </Form.Item>
      </Form>
      <Button onClick={beforeSubmit} type="primary" size="large" className="button-large-with">
        Войти
      </Button>
    </Spin>
  );
}

export default LoginUser;
