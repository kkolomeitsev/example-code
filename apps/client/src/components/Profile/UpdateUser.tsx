import React, { useState } from 'react';
import { Alert, Form, Button, Input, Spin, Row, Col, Card } from 'antd';
import NotAuthLinks from 'app/components/NotAuthLinks';
import MaskedInputPhone from '../MaskedInputPhone';
import { useProfileQuery } from '../Layouts/__generated__/ProfileQuery';
import {
  useUpdateUserMutation,
  UpdateUserMutationVariables as V,
} from './__generated__/UpdateUserMutation';

function UpdateUser() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const [form] = Form.useForm();

  const [mutate] = useUpdateUserMutation({
    refetchQueries: ['ProfileQuery'],
    notifications: { onCompleted: { title: 'Данные успешно изменены' } },
  });

  const { data, loading: loadingData } = useProfileQuery();

  const isAuth = data?.queue?.profile?._id;

  if (!loadingData && !isAuth) {
    return <NotAuthLinks />;
  }

  const profileData = data?.queue?.profile || null;

  const handleSubmit = async (values: any) => {
    setError('');
    setLoading(true);
    const input: V['input'] = {} as any;
    input['title'] = values.title;
    input['phone'] = values.phone;
    const result = await mutate({ variables: { input } });
    setLoading(false);
    if (!result?.data?.queue?.updateProfile?._id) {
      setError('Ошибка изменения данных');
    }
  };

  const beforeSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    form
      .validateFields()
      .then((values) => {
        handleSubmit(values);
      })
      .catch((_err) => null);
  };

  return (
    <Row gutter={16}>
      <Col span={24} offset={0} lg={{ span: 8, offset: 8 }}>
        <Card title="Профиль" bordered={false}>
          <Spin spinning={loading}>
            {error ? <Alert message={error} className="form-alert" type="error" /> : null}
            <Form
              className="login-form"
              layout="vertical"
              onFinish={handleSubmit}
              hideRequiredMark
              form={form}
              initialValues={profileData || {}}
            >
              <Form.Item
                label="Email адрес"
                name="username"
                rules={[{ required: true, message: 'Это обязательное поле!' }, { type: 'email' }]}
              >
                <Input placeholder="Ваш email" disabled />
              </Form.Item>
              <Form.Item
                label="Имя или название организации"
                name="title"
                rules={[{ required: true, message: 'Это обязательное поле!' }]}
              >
                <Input placeholder="Имя или название" />
              </Form.Item>
              <Form.Item
                label="Номер телефона"
                name="phone"
                rules={[{ required: true, message: 'Это обязательное поле!' }]}
              >
                <MaskedInputPhone placeholder="+7 ..." />
              </Form.Item>
            </Form>
            <Button
              onClick={beforeSubmit}
              type="primary"
              size="large"
              className="button-large-with"
            >
              Изменить данные
            </Button>
          </Spin>
        </Card>
      </Col>
    </Row>
  );
}

export default UpdateUser;
