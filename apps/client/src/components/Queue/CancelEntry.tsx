import React, { useState } from 'react';
import { Button } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import {
  useCancelEntryMutation,
  CancelEntryMutationVariables as V,
} from './__generated__/CancelEntryMutation';

function CancelEntry(props: { entryId: string; style?: React.CSSProperties; className?: string }) {
  const { entryId, style, className } = props;

  const [loading, setLoading] = useState(false);

  const [mutate] = useCancelEntryMutation({
    refetchQueries: ['ClientQueue'],
    notifications: {
      onCompleted: { title: 'Запись отменена' },
    },
  });

  const handleSubmit = (e: React.MouseEvent) => {
    if (loading) {
      return;
    }

    e.preventDefault();

    setLoading(true);

    const variables: V = {
      entryId,
    };

    mutate({ variables })
      .then((_result) => ({}))
      .catch((_e) => ({}))
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Button
      style={style}
      className={className}
      type="primary"
      onClick={handleSubmit}
      icon={<CloseCircleOutlined />}
      loading={loading}
    >
      Отменить
    </Button>
  );
}

export default CancelEntry;
