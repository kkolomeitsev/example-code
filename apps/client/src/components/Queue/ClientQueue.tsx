import React from 'react';
import { Spin, Typography, Col, Row, Card } from 'antd';
import NotAuthLinks from 'app/components/NotAuthLinks';
import { getTitle } from 'app/lib/getTitle';
import Head from 'next/head';
import vars from 'app/lib/vars';
import ClientQueueTable from './ClientQueueTable';
import QueueChange from './QueueChange';
import CreateEntry from './CreateEntry';
import { QueueStatus } from 'app/__generated__/types';
import {
  useClientQueue,
  ClientQueueEntryWithDate_data as QueueData,
} from './__generated__/ClientQueueQuery';

const { Title, Text } = Typography;

interface P {
  link: string;
}

function Queue(props: P) {
  const { link } = props;
  const { data: result, loading, refetch } = useClientQueue({
    variables: { link },
  });

  const isAuth = result?.queue?.profile?._id;

  if (!loading && !isAuth) {
    return <NotAuthLinks />;
  }

  const data = result?.queue?.queue || null;

  if (!data) {
    return <p>Очередь не найдена</p>;
  }

  const myTodayEntry = data?.myEntries?.find((el) => el?.isToday);
  const meCurrent =
    data?.currentEntry?.commonTurn && data.currentEntry.commonTurn === myTodayEntry?.commonTurn;
  const queueClosed = data?.status?.trim().toLowerCase() === 'closed';

  return (
    <>
      <Head>
        <title>
          Очередь{data?.title ? ` ${data.title}` : ''}
          {vars.sitename}
        </title>
      </Head>
      {link ? (
        <QueueChange queue={link} refetch={refetch} myTurn={myTodayEntry?.commonTurn} />
      ) : null}
      {link ? (
        <CreateEntry queue={data} hasToday={!!myTodayEntry} queueClosed={queueClosed} />
      ) : null}
      <br />
      <br />
      <Spin spinning={loading}>
        <div>
          <Title className="page-title" level={4}>
            Очередь {`"${data?.title || '<без названия>'}"`}
          </Title>
          <div className="mb3">
            <p>
              Статус:{' '}
              <span
                className={`queue-status-title status-${
                  data?.status?.trim().toLowerCase() || 'closed'
                }`}
              >
                {getTitle(data?.status).toUpperCase()}
              </span>
            </p>
            {data?.status !== QueueStatus.BREAK ? null : (
              <Text code>{data.breakMessage || 'Перерыв'}</Text>
            )}
          </div>
          {queueClosed ? null : (
            <Row gutter={[{ md: 16, lg: 16, xl: 32 }, 16]}>
              <Col span={24} md={12} lg={12} xl={12}>
                <Card
                  title={
                    <Title className="queue-title-l2" level={3}>
                      Сейчас на приёме
                    </Title>
                  }
                  className={
                    data?.currentEntry?.commonTurn &&
                    data.currentEntry.commonTurn === myTodayEntry?.commonTurn
                      ? 'my-turn'
                      : undefined
                  }
                >
                  {data?.currentEntry?.commonTurn ? (
                    <>
                      номер:{' '}
                      <span className={`queue-turn-title`}>{data.currentEntry.commonTurn}</span>
                    </>
                  ) : (
                    'Никого нет'
                  )}
                </Card>
              </Col>
              <Col span={24} md={12} lg={12} xl={12}>
                <Card
                  title={
                    <Title className="queue-title-l2" level={3}>
                      Очередь
                    </Title>
                  }
                  className="queue-table-card"
                >
                  <ClientQueueTable
                    data={(data?.entries as QueueData[]) || []}
                    breakEntry={data?.breakEntry || null}
                    myTodayEntry={myTodayEntry || null}
                    meCurrent={!!meCurrent}
                  />
                </Card>
              </Col>
            </Row>
          )}
        </div>
      </Spin>
    </>
  );
}

export default Queue;
