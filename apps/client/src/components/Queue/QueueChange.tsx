import React from 'react';
import useSound from 'use-sound';
import { useQueueChangeSubscription } from './__generated__/QueueChangeSubscription';

function QueueChange(props: { queue: string; myTurn?: number | null; refetch: () => void }) {
  const { queue, refetch } = props;

  const [play] = useSound('/static/sound/turn.mp3');

  useQueueChangeSubscription({
    variables: { queue },
    onSubscriptionData: ({ subscriptionData }) => {
      if (subscriptionData?.data?.queueChanges?.upd) {
        refetch();
        const newTurn = subscriptionData?.data?.queueChanges?.newTurn;
        if (newTurn && newTurn === props.myTurn) {
          play();
        }
      }
    },
  });

  return <span />;
}

export default QueueChange;
