import React, { useState, useEffect } from 'react';
import { Drawer, Alert, Form, Button, Spin, DatePicker, Switch } from 'antd';
import moment from 'moment';
import { PlusOutlined } from '@ant-design/icons';
import windowWidth from 'app/lib/windowWidth';
import { getErrorMessage } from 'app/lib/getErrorMessage';
import { ClientQueueData } from 'app/__generated__/types';
import {
  useCreateEntryMutation,
  CreateEntryMutationVariables as V,
} from './__generated__/CreateEntryMutation';

function CreateEntry(props: {
  queue: ClientQueueData | null;
  hasToday: boolean;
  queueClosed: boolean;
}) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();

  const [init, setInit] = useState(false);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (init && typeof window !== 'undefined') {
      setWidth(windowWidth() || 1);
    }
  }, [init]);

  if (!init && typeof window !== 'undefined') {
    setInit(true);
  }

  const [mutate] = useCreateEntryMutation({
    refetchQueries: ['ClientQueue'],
    notifications: { onCompleted: { title: 'Вы встали в очередь' } },
    onError: (e) => {
      setError(`Ошибка${e.message ? `: ${getErrorMessage(e.message)}!` : '!'}`);
    },
  });

  const onClose = () => {
    setVisible(false);
    setError('');
    form.resetFields();
  };

  const handleSubmit = (values: any) => {
    if (loading) {
      return;
    }

    setError('');
    setLoading(true);

    const variables: V = {
      input: {
        date: moment(values.date).utcOffset(0).toDate(),
        queue: { link: props.queue?.link },
        inLife: Boolean(values.inLife),
      },
    };

    mutate({ variables })
      .then((result) => {
        if (result) {
          setTimeout(() => {
            onClose();
          }, 500);
        }
      })
      .catch((e) => {
        setError(`Ошибка${e.message ? `: ${getErrorMessage(e.message)}` : '!'}`);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const beforeSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    form
      .validateFields()
      .then((values) => {
        handleSubmit(values);
      })
      .catch((_err) => null);
  };

  return (
    <>
      <Button type="primary" onClick={() => setVisible(true)}>
        <PlusOutlined /> Встать в очередь
      </Button>
      <Drawer
        title="Встать в очередь"
        width={width && width < 768 ? 250 : 450}
        visible={visible}
        onClose={onClose}
      >
        <Spin spinning={loading}>
          {error ? <Alert message={error} className="form-alert" type="error" /> : null}
          <Form
            className="queue-form"
            layout="vertical"
            onFinish={handleSubmit}
            hideRequiredMark
            initialValues={{ inLife: props.hasToday || props.queueClosed ? false : true }}
            form={form}
          >
            <Form.Item label="Дата, время" name="date" required={true}>
              <DatePicker
                format="YYYY-MM-DD HH:mm"
                showTime={{
                  hideDisabledOptions: true,
                  defaultValue: moment(moment().add(1, 'm').format('HH:mm'), 'HH:mm'),
                }}
                placeholder="Выберите дату, время"
                disabledDate={function disabledDate(current) {
                  return (
                    current <
                      moment()
                        .subtract(props.hasToday ? 0 : 1, 'd')
                        .endOf('day') ||
                    current >
                      moment()
                        .add(
                          typeof props.queue?.bookingPeriodDays === 'number'
                            ? props.queue.bookingPeriodDays
                            : 1,
                          'd'
                        )
                        .endOf('day') ||
                    (props.queue?.recurringWeekend || []).includes(String(current.weekday()))
                  );
                }}
                onChange={(value) => {
                  let newCurrent = moment().add(1, 'm');
                  if (props.hasToday) {
                    newCurrent = moment(newCurrent).add(1, 'd');
                  }
                  if (value && value.isBefore(newCurrent)) {
                    form.setFieldsValue({ date: newCurrent });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="Сразу встать в живую очередь" name="inLife" valuePropName="checked">
              <Switch />
            </Form.Item>
          </Form>
          <br />
          <br />
          <br />
        </Spin>
        <div className="drawer-form-footer">
          <Button
            onClick={beforeSubmit}
            type="primary"
            size="large"
            className="button-large-with"
            disabled={loading}
          >
            Сохранить
          </Button>
          <Button onClick={() => onClose()} style={{ marginLeft: 18 }}>
            Отмена
          </Button>
        </div>
      </Drawer>
    </>
  );
}

export default CreateEntry;
