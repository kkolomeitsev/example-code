import React, { useState } from 'react';
import { Button } from 'antd';
import { LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import {
  useUpdateEntryStatusMutation,
  UpdateEntryStatusMutationVariables as V,
} from './__generated__/UpdateEntryStatusMutation';

function UpdateEntryStatus(props: {
  entryId: string;
  inLife: boolean;
  style?: React.CSSProperties;
  className?: string;
}) {
  const { entryId, inLife, style, className } = props;

  const [loading, setLoading] = useState(false);

  const [mutate] = useUpdateEntryStatusMutation({
    refetchQueries: ['ClientQueue'],
    notifications: {
      onCompleted: { title: inLife ? 'Вы встали в живую очередь' : 'Вы вышили из живой очереди' },
    },
  });

  const handleSubmit = (e: React.MouseEvent) => {
    if (loading) {
      return;
    }

    e.preventDefault();

    setLoading(true);

    const variables: V = {
      entryId,
      inLife,
    };

    mutate({ variables })
      .then((_result) => ({}))
      .catch((_e) => ({}))
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Button
      className={className}
      style={style}
      type="primary"
      onClick={handleSubmit}
      loading={loading}
      icon={inLife ? <LoginOutlined /> : <LogoutOutlined />}
    >
      {inLife ? 'Встать в живую очередь' : 'Отойти'}
    </Button>
  );
}

export default UpdateEntryStatus;
