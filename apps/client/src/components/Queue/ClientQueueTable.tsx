import React from 'react';
import { Table, Tooltip, Divider, Typography } from 'antd';
import { ColumnProps } from 'antd/lib/table';
import { QuestionCircleOutlined } from '@ant-design/icons';
import UpdateEntryStatus from './UpdateEntryStatus';
import CancelEntry from './CancelEntry';
import {
  ClientQueueEntryWithDate_data as Data,
  ClientQueueEntry_data as Entry,
  ClientQueueMyEntry_data as MyEntry,
} from './__generated__/ClientQueueQuery';
import moment from 'moment';

const { Text } = Typography;

export default function ClientQueueTable(props: {
  data: Data[];
  breakEntry: Entry | null;
  myTodayEntry: MyEntry | null;
  meCurrent: boolean;
}) {
  const { data: importData, myTodayEntry, breakEntry, meCurrent } = props;
  const data = (JSON.parse(JSON.stringify(importData)) as Data[]).sort((a, b) =>
    moment(a.date).isBefore(b.date) ? -1 : moment(a.date).isSame(b.date) ? 0 : 1
  );

  const inLifeEntry = !myTodayEntry
    ? false
    : data.findIndex((el) => el._id === myTodayEntry._id) === -1
    ? false
    : true;

  const columns: ColumnProps<Data>[] = [
    {
      title: 'Записался на',
      key: 'time',
      align: 'left',
      width: '30%',
      render: (_, record) => (
        <span>
          <b>{moment(record.date).format('HH:mm')}</b>
        </span>
      ),
    },
    {
      title: 'Номер',
      dataIndex: 'commonTurn',
      key: 'commonTurn',
      width: '15%',
      render: (value: string) => (
        <span>
          <b>{value}</b>
        </span>
      ),
    },
    {
      title: '',
      className: 'queue-table-breaker',
      key: 'breaker',
      align: 'right',
      width: '55%',
      render: (_, record) =>
        record._id === breakEntry?._id ? (
          <Tooltip title="После данного клиента запланирован перерыв">
            <QuestionCircleOutlined className="ico-lg" />
          </Tooltip>
        ) : record._id === myTodayEntry?._id ? (
          <>
            <Tooltip title="Ваша очередь">
              <QuestionCircleOutlined className="ico-lg" />
            </Tooltip>
            <UpdateEntryStatus
              entryId={record._id}
              inLife={false}
              className="update-entry-status-btn"
            />
            <CancelEntry entryId={record._id} className="cancel-entry-btn" />
          </>
        ) : null,
    },
  ];

  return (
    <>
      {myTodayEntry && !inLifeEntry && !meCurrent ? (
        <>
          <div className="mb3">
            <Divider className="queue-table-divider-text" orientation="left">
              Ваша запись
            </Divider>
            <div className="queue-my-entry-text">
              <Text>{`${moment(myTodayEntry.date).format('HH:mm')} - Ваш номер очереди: `}</Text>{' '}
              <Text mark>&nbsp;{myTodayEntry.commonTurn}&nbsp;</Text>{' '}
              <UpdateEntryStatus
                entryId={myTodayEntry._id}
                inLife={true}
                className="update-entry-status-btn"
              />
              <CancelEntry entryId={myTodayEntry._id} className="cancel-entry-btn" />
            </div>
          </div>
          <Divider className="mt0" orientation="center">
            Очередь
          </Divider>
        </>
      ) : null}
      {props.data.length ? (
        <Table
          dataSource={data}
          rowKey="_id"
          columns={columns}
          pagination={false}
          locale={{ emptyText: 'Очередь пуста' }}
          rowClassName={(record) => {
            if (!myTodayEntry && !breakEntry) {
              return '';
            }
            if (record._id === myTodayEntry?._id) {
              return 'my-entry-row';
            }
            if (record._id === breakEntry?._id) {
              return 'break-entry-row';
            }
            return '';
          }}
        />
      ) : (
        <p className="empty-queue-table-text">Очередь пуста</p>
      )}
    </>
  );
}
