import { Input } from 'antd';
import { InputProps } from 'antd/lib/input';
import React, { forwardRef } from 'react';
import ReactInputMask from 'react-input-mask';

const MaskedInputPhone = forwardRef((props: InputProps, ref) => {
  return (
    <ReactInputMask {...(props as any)} mask="+7 (999) 999-99-99" alwaysShowMask={false}>
      {(inputProps: any) => <Input {...inputProps} ref={ref} />}
    </ReactInputMask>
  );
});

export default MaskedInputPhone;
