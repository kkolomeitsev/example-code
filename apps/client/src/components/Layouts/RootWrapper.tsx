import React, { FunctionComponent, ComponentProps, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Layout, Menu, ConfigProvider, Row, Col } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import ruRU from 'antd/lib/locale/ru_RU';
import windowWidth from 'app/lib/windowWidth';
import { useProfileQuery } from './__generated__/ProfileQuery';
import { useLogoutUserMutation } from './__generated__/LogoutUserMutation';
import Logo from 'app/assets/svg/simplelogo.svg';

const { Header, Content, Footer } = Layout;

const { SubMenu } = Menu;

function RootWrapper(props: ComponentProps<FunctionComponent>) {
  const { children } = props;

  const [init, setInit] = useState(false);
  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (init && typeof window !== 'undefined') {
      setWidth(windowWidth() || 1);
    }
  }, [init]);

  if (!init && typeof window !== 'undefined') {
    setInit(true);
  }

  const [mutate, { loading }] = useLogoutUserMutation({
    notifications: { onCompleted: false },
  });

  const { data } = useProfileQuery();

  const loggedIn = data?.queue?.profile?._id ? true : false;
  const profileData = loggedIn ? data?.queue?.profile : null;

  const router = useRouter();

  const loggedInMenu = [
    <Menu.Item key="/my-queue">
      <Link href="/my-queue">
        <a>Управление очередью</a>
      </Link>
    </Menu.Item>,
    <Menu.Item key="/profile">
      <Link href="/profile">
        <a>{profileData?.title || ''}</a>
      </Link>
    </Menu.Item>,
    <Menu.Item key="/logout">
      <a
        href="#"
        onClick={(e) => {
          e.preventDefault();
          if (!loading) {
            mutate().finally(() => {
              document.location.href = '/';
            });
          }
        }}
      >
        {loading ? <LoadingOutlined className="mr2" /> : null}
        Выйти
      </a>
    </Menu.Item>,
  ];

  const standardMenu = [
    <Menu.Item key="/register">
      <Link href="/register">
        <a>Регистрация</a>
      </Link>
    </Menu.Item>,
    <Menu.Item key="/login">
      <Link href="/login">
        <a>Войти</a>
      </Link>
    </Menu.Item>,
  ];

  let menuItems: JSX.Element[] = [];

  if (loggedIn) {
    menuItems = menuItems.concat(loggedInMenu);
  } else {
    menuItems = menuItems.concat(standardMenu);
  }

  const submenu = (
    <SubMenu key="SubMenu" title="Меню">
      {menuItems}
    </SubMenu>
  );

  return (
    <Layout style={{ minHeight: '100vh', maxWidth: '100%' }}>
      <Header className="header">
        <div className="header-logo-block">
          <Logo
            className="logo"
            onClick={() => {
              router.push('/');
              return;
            }}
          />
        </div>

        <div className="header-nav-block">
          <Menu
            theme="light"
            className="nav-block-menu"
            mode="horizontal"
            defaultSelectedKeys={[router.asPath]}
          >
            {width && width < 768 ? submenu : menuItems}
          </Menu>
        </div>
      </Header>

      <ConfigProvider locale={ruRU}>
        <Content className="content-wrapper">{children}</Content>
      </ConfigProvider>

      <Footer className="text-center">
        <div className="content-container footer-container">
          <Row>
            <Col span={24} md={6} lg={6} xl={6}>
              <Link href="/about">
                <a className="host-link  footer-link">О нас</a>
              </Link>
            </Col>
            <Col span={24} md={6} lg={6} xl={6}>
              <Link href="/payment-security">
                <a className="host-link footer-link">
                  Безопасность
                  <br />
                  онлайн-платежей
                </a>
              </Link>
            </Col>
            <Col span={24} md={6} lg={6} xl={6}>
              <Link href="/doc">
                <a className="host-link footer-link">Документация</a>
              </Link>
            </Col>
            <Col span={24} md={6} lg={6} xl={6}>
              <Link href="/contact">
                <a className="host-link footer-link">Обратная связь</a>
              </Link>
            </Col>
          </Row>
        </div>
        SimpleQ ©2020 •{' '}
        <Link href="/terms/privacy">
          <a className="host-link">Политика конфиденциальности</a>
        </Link>{' '}
        •{' '}
        <Link href="/terms/common">
          <a className="host-link">Пользовательское соглашение</a>
        </Link>
      </Footer>
    </Layout>
  );
}

export default RootWrapper;
