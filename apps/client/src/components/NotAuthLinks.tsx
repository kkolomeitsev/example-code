import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

function NotAuthLinks() {
  const router = useRouter();

  const returnUrl = router.asPath;

  return (
    <div>
      <p>
        Вы не авторизованы, пожалуйста{' '}
        <Link href={`/login?return_url=${encodeURI(returnUrl)}`}>
          <a>войдите</a>
        </Link>{' '}
        или{' '}
        <Link href={`/register?return_url=${encodeURI(returnUrl)}`}>
          <a>зарегистрируйтесь</a>
        </Link>
      </p>
    </div>
  );
}

export default NotAuthLinks;
