import notification from 'antd/lib/notification';
import 'antd/lib/notification/style';

export type NotificationType = 'success' | 'error' | 'info' | 'warn' | 'warning';

export interface Notification {
  type: NotificationType;
  message: string;
  title: string;
}

export function NotificationWithIcon({ type, title, message }: Notification) {
  // NotificationWithIcon uses global `document` variable
  // which is not accesible on NodeJS environment
  if (typeof document !== 'undefined') {
    notification[type]({
      message: title,
      description: message,
    });
  }
}
