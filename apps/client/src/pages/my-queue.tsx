import React from 'react';
import { Col, Row } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import MyQueue from 'app/components/MyQueue/MyQueue';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

function MyQueuePage() {
  return (
    <RootWrapper>
      <Head>
        <title>Управление очередью{vars.sitename}</title>
      </Head>
      <div className="content">
        <Row gutter={16}>
          <Col span={24}>
            <MyQueue />
          </Col>
        </Row>
      </div>
    </RootWrapper>
  );
}

export default withApollo(MyQueuePage);
