import React from 'react';
import { Card, Col, Row } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import LoginUser from 'app/components/Login/LoginUser';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

function LoginPage() {
  return (
    <RootWrapper>
      <Head>
        <title>Авторизация{vars.sitename}</title>
      </Head>
      <div className="content">
        <Row gutter={16}>
          <Col span={24} offset={0} lg={{ span: 8, offset: 8 }}>
            <Card title="Авторизация" bordered={false}>
              <LoginUser />
            </Card>
          </Col>
        </Row>
      </div>
    </RootWrapper>
  );
}

export default withApollo(LoginPage);
