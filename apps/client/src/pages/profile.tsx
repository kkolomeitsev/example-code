import React from 'react';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import UpdateUser from 'app/components/Profile/UpdateUser';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

function ProfilePage() {
  return (
    <RootWrapper>
      <Head>
        <title>Профиль{vars.sitename}</title>
      </Head>
      <div className="content">
        <UpdateUser />
      </div>
    </RootWrapper>
  );
}

export default withApollo(ProfilePage);
