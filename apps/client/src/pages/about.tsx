import React from 'react';
import { Typography, Card } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

const { Title, Paragraph } = Typography;

function AboutPage() {
  return (
    <RootWrapper>
      <Head>
        <title>О нас{vars.sitename}</title>
      </Head>
      <div className="content content-container">
        <Card>
          <Title level={1}>О нас</Title>
          <Paragraph code>Раздел в разработке</Paragraph>
        </Card>
      </div>
    </RootWrapper>
  );
}

export default withApollo(AboutPage);
