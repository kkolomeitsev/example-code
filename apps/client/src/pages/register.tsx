import React from 'react';
import { Card, Col, Row } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import RegisterUser from 'app/components/Register/RegisterUser';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

function RegisterPage() {
  return (
    <RootWrapper>
      <Head>
        <title>Регистрация{vars.sitename}</title>
      </Head>
      <div className="content">
        <Row gutter={16}>
          <Col span={24} offset={0} lg={{ span: 8, offset: 8 }}>
            <Card title="Регистрация" bordered={false}>
              <RegisterUser />
            </Card>
          </Col>
        </Row>
      </div>
    </RootWrapper>
  );
}

export default withApollo(RegisterPage);
