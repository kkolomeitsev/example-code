import React from 'react';
import { Col, Row } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import { useRouter } from 'next/router';
import { withApollo } from 'app/lib/withApollo';
import ClientQueue from 'app/components/Queue/ClientQueue';

function QueuePage() {
  const router = useRouter();

  const { link: linkQuery } = router.query;
  let link = '';
  if (Array.isArray(linkQuery)) {
    if (linkQuery.length) {
      link = linkQuery[0];
    }
  } else if (linkQuery) {
    link = linkQuery;
  }

  return (
    <RootWrapper>
      <div className="content">
        <Row gutter={16}>
          <Col span={24}>{!link ? 'Неправильная ссылка' : <ClientQueue link={link} />}</Col>
        </Row>
      </div>
    </RootWrapper>
  );
}

export default withApollo(QueuePage);
