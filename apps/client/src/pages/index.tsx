import React from 'react';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import Link from 'next/link';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

function IndexPage() {
  return (
    <RootWrapper>
      <Head>
        <title>{vars.sitename.substring(3)}</title>
        <meta
          name="description"
          content="Сервис онлайн записи и бронирования очередей в Казахстане"
        />
      </Head>
      <div id="landing-content">
        <div className="content-container card-container first-card-container">
          <div className="title-header">
            <div className="title-text">
              <h1>Онлайн&nbsp;очереди</h1>
              <h2>запись&nbsp;и&nbsp;бронирование</h2>
              <h4>
                Надоело стоять в очередях?
                <br />
                Приходишь по записи и всё равно приходится ждать?
                <br />
                Устал от сложных и неудобных интерфейсов?
              </h4>
              <h2 className="appeal">SimpleQ создан специально для тебя!</h2>
            </div>
          </div>
        </div>

        <div className="content-container">
          <h2 className="block-title-text">3 простых шага</h2>
          <h2 className="block-subtitle-text">чтобы встать в очередь</h2>

          <div className="flex-row">
            <div className="flex-col text-center">
              <div className="how-to-img reg-login-img" />
              <p className="col-title-p">
                <Link href="/register">
                  <a>Зарегистрироваться</a>
                </Link>{' '}
                /{' '}
                <Link href="/login">
                  <a>войти</a>
                </Link>
              </p>
            </div>
            <div className="flex-col text-center">
              <div className="how-to-img link-to-img" />
              <p className="col-title-p">Перейти на страницу очереди</p>
            </div>
            <div className="flex-col text-center">
              <div className="how-to-img booking-to-img" />
              <p className="col-title-p">Встать в очередь или записаться</p>
            </div>
          </div>
        </div>
      </div>
    </RootWrapper>
  );
}

export default withApollo(IndexPage);
