import React from 'react';
import { Typography, Card } from 'antd';
import RootWrapper from 'app/components/Layouts/RootWrapper';
import { withApollo } from 'app/lib/withApollo';
import Head from 'next/head';
import vars from 'app/lib/vars';

const { Title, Paragraph } = Typography;

function ContactPage() {
  return (
    <RootWrapper>
      <Head>
        <title>Обратная связь{vars.sitename}</title>
      </Head>
      <div className="content content-container">
        <Card>
          <Title level={1}>Обратная связь</Title>
          <Paragraph>
            По всем вопросам пишите нам
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;в телеграм: <a href="https://t.me/simpleq_kz">@simpleq_kz</a>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;на почту: <a href="mailto:info@simpleq.kz">info@simpleq.kz</a>
          </Paragraph>
          <Paragraph code>Остальная часть раздела в разработке</Paragraph>
        </Card>
      </div>
    </RootWrapper>
  );
}

export default withApollo(ContactPage);
