/* eslint-disable @typescript-eslint/no-var-requires */
// eslint-disable-next-line @typescript-eslint/no-empty-function
require.extensions['.css'] = (_file) => {};
// eslint-disable-next-line @typescript-eslint/no-empty-function
require.extensions['.less'] = (_file) => {};

require('dotenv').config({
  path: require('path').resolve(process.cwd(), '.env.dev'),
  debug: !!process.env.DEBUG,
});

require('module-alias').addAlias('app', __dirname);
const express = require('express');
const next = require('next');
const fs = require('fs');
const path = require('path');
const https = require('https');
const http = require('http');
global.fetch = require('isomorphic-unfetch');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const cfg = {
  dev,
  dir: dev ? './src' : path.resolve(__dirname),
  conf: {
    distDir: './dist',
    poweredByHeader: false,
    publicRuntimeConfig: {
      APP_GRAPHQL_URI: process.env.APP_GRAPHQL_URI,
      APP_GRAPHQL_WS_URI: process.env.APP_GRAPHQL_WS_URI,
      ENV_TYPE: process.env.ENV_TYPE,
    },
    ...(dev ? require('./next.config.js') : null),
  },
};
const app = next(cfg);
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  if (!dev) {
    server.enable('trust proxy');
  }

  server.disable('X-Powered-By');

  server.use(function (req, res, next) {
    res.removeHeader('X-Powered-By');
    next();
  });

  // server.use('/static', express.static('static', { etag: true }));
  server.use(express.static(path.resolve(__dirname, 'public'), { etag: true }));

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  const httpsOptions = {};
  let serverApp = server;

  if (!process.env.IS_LOCAL_WATCH) {
    // httpsOptions.cert = fs.readFileSync(path.join(__dirname, '../../simpleq.kz.crt'));
    // httpsOptions.key = fs.readFileSync(path.join(__dirname, '../../simpleq.kz.key'));
    serverApp = https.createServer(httpsOptions, server);
  } else {
    serverApp = http.createServer(server);
  }

  serverApp.listen(port, (err) => {
    if (err) throw err;
    console.log(`🚨 🚀 Client SSR app ready at http://localhost:${port}`);
  });
});
