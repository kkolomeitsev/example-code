/* eslint-disable */
const path = require('path');
const fs = require('fs');
const withFonts = require('next-fonts');
const withImages = require('next-images');
const withLess = require('@zeit/next-less');
const lessToJS = require('less-vars-to-js');

if (typeof require !== 'undefined') {
  require.extensions['.css'] = (file) => {};
  require.extensions['.scss'] = (file) => {};
  require.extensions['.less'] = (file) => {};
}

const themeVariables = lessToJS(
  fs.readFileSync(path.resolve(__dirname, './src/assets/style/antd-custom.less'), 'utf8')
);

const config = withLess(
  withImages(
    withFonts({
      cssModules: false,
      distDir: '../dist',
      imageTypes: ['jpg', 'png'],
      inlineImageLimit: 16384,
      enableSvg: true,
      lessLoaderOptions: {
        javascriptEnabled: true,
        modifyVars: themeVariables,
      },
      webpack: (config, { isServer, dev }) => {
        if (isServer) {
          const antStyles = /antd\/.*?\/style.*?/;
          const origExternals = [...config.externals];
          config.externals = [
            (context, request, callback) => {
              if (request.match(antStyles)) return callback();
              if (typeof origExternals[0] === 'function') {
                origExternals[0](context, request, callback);
              } else {
                callback();
              }
            },
            ...(typeof origExternals[0] === 'function' ? [] : origExternals),
          ];

          config.module.rules.unshift({
            test: antStyles,
            use: 'null-loader',
          });
        }

        const builtInLoader = config.module.rules.find((rule) => {
          if (rule.oneOf) {
            return (
              rule.oneOf.find((deepRule) => {
                if (deepRule.test && deepRule.test.toString().includes('/a^/')) {
                  return true;
                }
                return false;
              }) !== undefined
            );
          }
          return false;
        });

        if (typeof builtInLoader !== 'undefined') {
          config.module.rules.push({
            oneOf: [
              ...builtInLoader.oneOf.filter((rule) => {
                return (rule.test && rule.test.toString().includes('/a^/')) !== true;
              }),
            ],
          });
        }

        config.resolve.alias['@'] = path.resolve(__dirname);
        return config;
      },
    })
  )
);

module.exports = config;
